#!/usr/bin/python

# ./ul_drv_fsm2graph.py ul_drv/ul_drv/*.c && dot -Tps out.dot -o out.ps && epstopdf out.ps && kpdf out.pdf
# ./ul_drv_fsm2graph.py ul_drv/ul_drv/ul_di.c ul_drv/ul_drv/ul_frame.c ul_drv/ul_drv/ul_iac.c ul_drv/ul_drv/ul_ufsm.c && dot -Tps out.dot -o out.ps && epstopdf out.ps && kpdf out.pdf
from __future__ import with_statement
import os, sys, re, string


class ul_drv_fsm(object):

  def __init__ (self):
    self.func_tab={}
    self.next_tab={}
    self.call_tab={}
    self.call2_tab={}
    self.ret_tab={}
    self.entry_tab={}

  def parse_file(self,fname):

    re_fname=re.compile(".*int *([_A-Za-z]\w*) *\( *ul_drv.*")
    re_fnext=re.compile("UL_FNEXT *\(([^(),]*)\)")
    re_fcall=re.compile("UL_FCALL *\(([^(),]*)\)")
    re_fcall2=re.compile("UL_FCALL2 *\(([^(),]*),([^(),]*)\)")
    re_fret=re.compile("UL_FRET")
    re_comment=re.compile(".*/\* *([^ ].*[^ ]) *\*/.*")

    new_node_store_fl=False
    with open(fname,'r') as f:
        for line in f:
            if line[-1]=="\n":
                line=line[0:-1]

            node_fl = False
            pieces=re_fname.split(line)
            if len(pieces)>=2:
	      new_node_store_fl=True
	      node_name = pieces[1]
	      #print 'Node: '+pieces[1]

            pieces=re_fnext.split(line)
            if len(pieces)>=2:
	      node_fl=True
              comment=None;
              if len(pieces)>=3:
                comment_pieces=re_comment.split(pieces[2])
                if len(comment_pieces)>=2:
                  comment=comment_pieces[1]
	      if node_name not in self.next_tab:
	        self.next_tab[node_name]=[]
	      self.next_tab[node_name].append((pieces[1],comment))
	      #print '  Next: '+pieces[1]

            pieces=re_fcall.split(line)
            if len(pieces)>=2:
	      node_fl=True
              comment=None;
              if len(pieces)>=3:
                comment_pieces=re_comment.split(pieces[2])
                if len(comment_pieces)>=2:
                  comment=comment_pieces[1]
	      if node_name not in self.call_tab:
	        self.call_tab[node_name]=[]
	      self.call_tab[node_name].append((pieces[1],comment))
	      if pieces[1] not in self.entry_tab:
	        self.entry_tab[pieces[1]]=1
	      #print '  Call: '+pieces[1]

            pieces=re_fcall2.split(line)
            if len(pieces)>=3:
	      node_fl=True
              comment=None;
              if len(pieces)>=4:
                comment_pieces=re_comment.split(pieces[3])
                if len(comment_pieces)>=2:
                  comment=comment_pieces[1]
	      if node_name not in self.call2_tab:
	        self.call2_tab[node_name]=[]
	      self.call2_tab[node_name].append((pieces[1],pieces[2],comment))
	      if pieces[1] not in self.entry_tab:
	        self.entry_tab[pieces[1]]=1
	      #print '  Call: '+pieces[1]
	      #print '  Next: '+pieces[2]

            if re_fret.search(line) != None:
	      node_fl=True
	      if node_name not in self.ret_tab:
	        self.ret_tab[node_name]=1
	      #print '  Ret:'

            if node_fl & new_node_store_fl:
	      new_node_store_fl = False
	      self.func_tab[node_name]=node_name
	    

    return True

  def get_cont_graph(self, anchor_funcs):
    toproc=anchor_funcs
    outlist=[]

    while toproc:
      if False:
        print 'toproc ='
        print  toproc
        print 'outlist ='
        print  outlist
      node=toproc.pop()
      if node not in outlist:
        outlist.append(node)
        if node in self.next_tab:
          for nn in self.next_tab[node]:
	    n=nn[0]
            if n not in outlist:
              toproc.append(n)
        if node in self.call2_tab:
          for cn in self.call2_tab[node]:
            n=cn[1]
            if n not in outlist:
              toproc.append(n)

    return outlist

  def name2dot(self, n):
    if n[0]=='*':
      n=n.replace('*udrv->', 'UDRV_')
    return n

  def func_entry_first(self):
    fncs=[]
    for fnc in self.entry_tab:
      if fnc in self.func_tab:
        fncs.append(self.func_tab[fnc])
    for fnc in self.func_tab:
      if fnc not in self.entry_tab:
        fncs.append(self.func_tab[fnc])
    return fncs

  def gen_graph(self, dotfname='out.dot', anchor_funcs=None, export_comment=False):

    if False:
      print 'Nodes: '
      print self.func_tab
      print 'Nexts: '
      print self.next_tab
      print 'Calls: '
      print self.call_tab
      print 'Call2s: '
      print self.call2_tab

    dotf=open(dotfname,'w')
    dotf.write('digraph FSM {\n')

    if anchor_funcs == None:
      list_fncs = self.func_entry_first()
    else:
      list_fncs = self.get_cont_graph(anchor_funcs)

    for fnc in list_fncs:
      print 'Function '+fnc
      if fnc not in self.ret_tab:
        dot_shape='ellipse'
      else:
        dot_shape='octagon'
      if fnc not in self.entry_tab:
        dotf.write(' '+fnc+' [shape='+dot_shape+'];\n')
      else:
        dotf.write(' '+fnc+' [shape='+dot_shape+', style=filled];\n')
      if fnc in self.call_tab:
        for x in self.call_tab[fnc]:
	  s=' Call '+x[0];
	  if x[1]:
	    s+='   \t"'+x[1]+'"'
          print s
	  callprops=''
	  callnode='_fnc_1_'+fnc+'_calls_'+self.name2dot(x[0]);
	  calllabel=self.name2dot(x[0])
	  if x[1] and export_comment:
	    callprops+=', comment="'+x[1]+'"'
	    calllabel+='\\n'+x[1]
          dotf.write('  '+callnode+' [shape=box, label="'+calllabel+'"'+callprops+'];\n')
          dotf.write('  '+fnc+' -> '+callnode+' ;\n')
          dotf.write('  '+callnode+' -> '+fnc+' ;\n')
      if fnc in self.call2_tab:
        for x in self.call2_tab[fnc]:
	  s=' Call2 '+x[0]+' '+x[1]
	  if x[2]:
	    s+='   \t"'+x[2]+'"'
          print s
	  callprops=''
	  callnode='_fnc_2_'+fnc+'_calls_'+self.name2dot(x[0]);
	  calllabel=self.name2dot(x[0])
	  if x[2] and export_comment:
	    callprops+=', comment="'+x[2]+'"'
	    calllabel+='\\n'+x[2]
          dotf.write('  '+callnode+' [shape=box, label="'+calllabel+'"'+callprops+'];\n')
          dotf.write('  '+fnc+' -> '+callnode+' ;\n')
          dotf.write('  '+callnode+' -> '+self.name2dot(x[1])+' ;\n')
      if fnc in self.next_tab:
        for x in self.next_tab[fnc]:
	  s=' Next '+x[0];
	  if x[1]:
	    s+='   \t"'+x[1]+'"'
          print s
	  nextprops=''
	  nextnode=self.name2dot(x[0])
	  if x[1] and export_comment:
	    nextprops+=', comment="'+x[1]+'"'
	    # nextprops+=', label="'+x[1]+'"'
	  if len(nextprops):
	    if nextprops[0]==',':
	      nextprops=nextprops[1:];
	    nextprops=' ['+nextprops+']'
          dotf.write('  '+fnc+' -> '+nextnode+nextprops+' ;\n')

    dotf.write('}\n')

    return True



if __name__ == '__main__':
  help_msg = '''SYNOPSIS: ul_drv_fsm2graph.py source.c ...
        generate state automatas '''

  argv = sys.argv[1:]

  opt_help = False
  opt_dotfname = 'out.dot'
  opt_anchor_funcs = None
  opt_export_comment = False

  if len(argv) == 0:
      opt_help = True
  args = []
  skip = 0
  for i in range(0, len(argv)):
    if skip: skip = skip-1; continue
    a = argv[i]
    if a == '-h':
        opt_help = True
        break
    elif a == '-a':
        opt_anchor_funcs = string.split(argv[i+1], ',')
        skip = 1
        continue
    elif a == '-o':
        opt_dotfname = argv[i+1]
        skip = 1
        continue
    elif a == '-c':
        opt_export_comment = True
        continue
    args.append(a)

  if opt_help:
    print 
    print help_msg
    print 
    sys.exit(0)

  fsm = ul_drv_fsm()
    
  for a in args:
    fsm.parse_file(a)

  fsm.gen_graph(opt_dotfname, opt_anchor_funcs, opt_export_comment)
