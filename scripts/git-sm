#!/bin/sh

IS_CLEAN="false"

while [ $# -gt 0 ] ; do
  case "${1}" in
    "--help" | "-h" )
      echo "Usage: `basename ${0}` [options] [git command|-e shell command]"
      echo "      -h --help       help"
      echo "      -w --whole      find top repository for whole project"
      echo "      -d --dirty      run the git command only in dirty repository"
      echo "      -e --external   do not prepend 'git' to the command line"
      echo "      --recursive     Find submodules recursively"
      exit 0
      ;;
    "--whole" | "-w" )
      OPT_WHOLE="true"
      shift 1
      ;;
    "--dirty" | "-d" )
      IS_CLEAN="git diff-index --quiet HEAD && git diff-files --quiet"
      shift 1
      ;;
    "--external" | "-e" )
      OPT_EXTERNAL=1
      shift 1
      ;;
    "--recursive" )
      OPT_RECURSIVE=--recursive
      shift 1
      ;;
    -* )
      echo "$(basename $0): unknown option $1" >&2
      exit 1
      ;;
    * )
      break
      ;;
  esac
done

export IS_CLEAN

[ -n "$OPT_EXTERNAL" ] || SM_COMMAND="git"
if [ -n "$SM_COMMAND" -o $# -ge 1 ] ; then
 for i in "$@" ; do
   if [ -z "$SM_COMMAND" ] ; then
     SM_COMMAND="\"$i\""
   else
     SM_COMMAND="$SM_COMMAND \"$i\""
   fi
 done
fi

export SM_COMMAND

if [ -n "${OPT_WHOLE}" ] ; then
 SM_TOP_DIR="$( old_pwd="" ;  while [ ! -e .gitmodules -o ! -d .git ] ; do if [ "$old_pwd" = `pwd`  ] ; \
   then exit 1 ; else old_pwd=`pwd` ; cd -P .. 2>/dev/null ; fi ; done ; pwd )"
 if [ -z "$SM_TOP_DIR" ] ; then
   echo "$(basename $0): not in git submodules tree" >&2
   exit 1
 fi
 cd "$SM_TOP_DIR"
fi

if ! git rev-parse --is-inside-work-tree >/dev/null ; then
 echo >&2 "$(basename $0): Not in git directory" ; exit 1
fi

cd "$( pwd )/$( git rev-parse --show-cdup )"

git submodule foreach $OPT_RECURSIVE \
 'unset GIT_DIR ; unset GIT_OBJECT_DIRECTORY ; \
  eval "$IS_CLEAN" || eval "$SM_COMMAND" ;  true'

echo "Entering superproject"
eval "$IS_CLEAN" || eval "$SM_COMMAND"
