#!/usr/bin/python

from __future__ import with_statement
import os, sys, re, string

class OIDUniqError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

class uloi_dict_gen_options(object):
  wrcontext_by_objdes = False
  exec_wrfnc_by_aoid = False
  context_prefix = None  
  fnc_prefix = None  
  type_prefix = None
  skip_oid_under = 0  

class uloi_dict_entry(object):
  rd_fl = False
  wr_fl = False
  oid = None
  oid_enum = None
  aoid = None
  valtype = None
  valform = None
  rdfnc = None
  rdcontext = None
  wrfnc = None
  wrcontext = None

  def show(self):
    access = ''
    if self.rd_fl:
      access += 'R'
    if self.wr_fl:
      access += 'W'
    print "oid=%d aoid=%s valtype=%s valform=%s %s"%(
           self.oid, self.aoid, self.valtype, self.valform,
           access)

  def regularize(self,options):
    if options.fnc_prefix:
      fnc_prefix = options.fnc_prefix
    else:
      fnc_prefix = 'uloi_'

    if options.context_prefix:
      context_prefix = options.context_prefix
    else:
      context_prefix = 'prop_data_'

    wrcontext_by_objdes = options.wrcontext_by_objdes

    exec_wrfnc_by_aoid = options.exec_wrfnc_by_aoid

    if not self.rdfnc and self.rd_fl:
      self.rdfnc = fnc_prefix+self.valtype+'_rdfnc'
    if not self.rdcontext and self.rd_fl:
      self.rdcontext = '&'+context_prefix+self.aoid

    if not self.wrfnc and self.wr_fl:
      if not exec_wrfnc_by_aoid or not (self.valtype == 'e'):
        self.wrfnc = fnc_prefix+self.valtype+'_wrfnc'
      else:
        self.wrfnc = fnc_prefix+self.aoid+'_execfnc'

    if not self.wrcontext and self.wr_fl:
      if not wrcontext_by_objdes:
        self.wrcontext = '&'+context_prefix+self.aoid
      else:
        if not self.rdcontext:
          self.rdcontext = '&'+context_prefix+self.aoid
        self.wrcontext = '&uloid_objdes_'+self.aoid

    if not self.oid_enum:
      self.oid_enum = 'I_'+self.aoid

  def gen_oid_enum(self, f_out):
    s = '#define '+self.oid_enum
    if len(s) < 16:
      s += '\t'
    f_out.write(s+'\t'+str(self.oid)+'\n')

  def gen_objdes(self, f_out):
    # ULOI_GENOBJDES(TEST,I_TEST,"s2",uloi_int_rdfnc,&test_val,uloi_int_wrfnc,&test_val)
    combtype = self.valtype
    if self.valform:
      combtype += '/' + self.valform
    combtype = '"'+combtype+'"'
    rdfnc = self.rdfnc
    rdcontext = self.rdcontext
    wrfnc = self.wrfnc
    wrcontext = self.wrcontext
    if not rdfnc:
      rdfnc = 'NULL_CODE'
    if not rdcontext:
      rdcontext = 'NULL'
    if not wrfnc:
      wrfnc = 'NULL_CODE'
    if not wrcontext:
      wrcontext = 'NULL'
    f_out.write('ULOI_GENOBJDES('+self.aoid+','+self.oid_enum+','+combtype+','+
                 rdfnc+','+rdcontext+','+wrfnc+','+wrcontext+')\n')

  def gen_oivar(self, f_out, options):
    var = self.rdcontext
    if not var:
      var = self.wrcontext
    if not var:
      return

    if options.type_prefix:
      type_prefix = options.type_prefix
    else:
      type_prefix = 'uloi_'

    s = type_prefix+self.valtype+'_t'
    if var[0]=='&':
      var = var[1:]
    f_out.write(s+' '+var+';\n')


class uloi_dict(object):

  def __init__ (self):
    self.dict_tab={}
    self.dict_by_aoid={}
    self.dict_by_oid={}
    self.uniqid = 0

  def parse_file(self,fname):
    # 231 deslen=15 name="GRAD_B" type="u2/*256"
    re_oidline=re.compile(' *([0-9]+) +deslen=([0-9]+) +name="([^"]*)" type="([^"]*)"')
    re_oidin_start=re.compile("module recognized commands and set vars:")
    re_oidout_start=re.compile("module recognized get vars:")
    in_fl = False
    out_fl = False

    new_node_store_fl=False
    with open(fname,'r') as f:
      for line in f:
        if line[-1]=="\n":
          line=line[0:-1]

        if re_oidin_start.match(line):
          in_fl=True
          out_fl=False
          continue

        if re_oidout_start.match(line):
          in_fl=False
          out_fl=True
          continue

        pieces=re_oidline.split(line)
        if len(pieces)>=2:
          oid = int(pieces[1])
          deslen = int(pieces[2])
          aoid = pieces[3]
          valtype = pieces[4].split('/',1)
          if(len(valtype) >= 2):
              valform = valtype[1]
          else:
              valform = None
          valtype = valtype[0]

          ouni = None
          if oid in self.dict_by_oid:
            ouni = self.dict_by_oid[oid]

          if aoid in self.dict_by_aoid:
            if ouni and (ouni != self.dict_by_aoid[aoid]):
              raise OIDUniqError(aoid)   
            ouni = self.dict_by_aoid[aoid]

          if ouni is None:
            ouni = self.uniqid
            self.uniqid += 1


          if not (ouni in self.dict_tab):
            self.dict_tab[ouni] = uloi_dict_entry()
            dictent = self.dict_tab[ouni]
            dictent.rd_fl = out_fl
            dictent.wr_fl = in_fl
            dictent.oid = oid
            dictent.aoid = aoid
            dictent.valtype = valtype
            dictent.valform = valform
          else:
            dictent = self.dict_tab[ouni]
            dictent.rd_fl |= out_fl
            dictent.wr_fl |= in_fl

            if not dictent.oid:
              dictent.oid = oid
            else:
              if dictent.oid != oid:
                raise OIDUniqError(dictent)   

            if not dictent.aoid:
              dictent.aoid = aoid
            else:
              if dictent.aoid != aoid:
                raise OIDUniqError(dictent)   
              
            if not dictent.valtype:
              dictent.valtype = valtype
            else:
              if dictent.valtype != valtype:
                raise OIDUniqError(dictent)   

            if not dictent.valform:
              dictent.valform = valform
            else:
              if dictent.valform != valform:
                raise OIDUniqError(dictent)   

          if oid:
            self.dict_by_oid[oid] = ouni

          if aoid:
            self.dict_by_aoid[aoid] = ouni

    return True

  def regularize(self,options):
     for ouni in self.dict_tab:
       dictent = self.dict_tab[ouni]
       dictent.regularize(options)

  def gen_oiinc(self, oiincfname, gen_options):
     #for ouni in self.dict_tab:
     #  self.dict_tab[ouni].show()

     if oiincfname:
       f_out=open(oiincfname,'w')
     else:
       f_out=sys.stdout

     f_out.write('/* file generated by oidict-tool */\n\n')

     for i in sorted(self.dict_by_oid):
       dictent = self.dict_tab[self.dict_by_oid[i]]
       dictent.gen_oid_enum(f_out)

     f_out.write('\n')

     for i in sorted(self.dict_by_oid):
       dictent = self.dict_tab[self.dict_by_oid[i]]
       dictent.gen_objdes(f_out)

     f_out.write('\n')

     f_out.write('#define OBJDES_LIST \\\n')
     l = sorted(self.dict_by_oid)
     for i in l:
       dictent = self.dict_tab[self.dict_by_oid[i]]
       if gen_options.skip_oid_under:
         if dictent.oid < gen_options.skip_oid_under:
           continue
       f_out.write('  &uloid_objdes_'+dictent.aoid)
       if i != l[-1]:
         f_out.write(', \\\n')
     f_out.write('\n')

  def gen_oivar(self, oivarfname, gen_options):
     #for ouni in self.dict_tab:
     #  self.dict_tab[ouni].show()

     if oivarfname:
       f_out=open(oivarfname,'w')
     else:
       f_out=sys.stdout

     f_out.write('/* variable prototypes by oidict-tool */\n\n')

     for i in sorted(self.dict_by_oid):
       dictent = self.dict_tab[self.dict_by_oid[i]]
       dictent.gen_oivar(f_out, gen_options)

if __name__ == '__main__':
  help_msg = '''SYNOPSIS: oidict-tool.py dictionary-text.oidict ...
        generate object dictionary representation '''

  argv = sys.argv[1:]

  opt_help = False
  opt_oiincfname = None
  opt_oivarfname = None

  if len(argv) == 0:
      opt_help = True
  args = []
  skip = 0
  for i in range(0, len(argv)):
    if skip: skip = skip-1; continue
    a = argv[i]
    if a == '-h':
        opt_help = True
        break
    elif a == '-o':
        opt_oiincfname = argv[i+1]
        skip = 1
        continue
    elif a == '-v':
        opt_oivarfname = argv[i+1]
        skip = 1
        continue
    args.append(a)

  if opt_help:
    print 
    print help_msg
    print 
    sys.exit(0)

  gen_options = uloi_dict_gen_options()

  oidict = uloi_dict()

  for a in args:
    oidict.parse_file(a)

  gen_options.wrcontext_by_objdes = True
  gen_options.exec_wrfnc_by_aoid = True
  gen_options.context_prefix = 'prop_' 
  gen_options.fnc_prefix = 'prop_'
  gen_options.type_prefix = 'prop_'
  gen_options.skip_oid_under = 100

  oidict.regularize(gen_options)

  oidict.gen_oivar(opt_oivarfname, gen_options)

  oidict.gen_oiinc(opt_oiincfname, gen_options)
