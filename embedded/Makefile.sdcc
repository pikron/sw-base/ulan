#  Makefile.rules - OCERA make framework common project rules -*- makefile-gmake -*- #OMK:base.omk
#
#  (C) Copyright 2003, 2006, 2007, 2008, 2009  by Pavel Pisa - OCERA team member
#  (C) Copyright 2006, 2007, 2008, 2009, 2010 by Michal Sojka - Czech Technical University, FEE, DCE
#
#  Homepage: http://rtime.felk.cvut.cz/omk/
#  Version:  0.2-35-gb9c630c
#
# The OMK build system is distributed under the GNU General Public
# License.  See file COPYING for details.
#
#
#                   Version for SDCC builds.                                     #OMK:sdcc-defines.omk
#
# Variables CC (sdcc), AR (sdcclib), AS (sdas8051), HC (packihx), HEX2BIN
# can be overridden by definition in config.omk
#
# input variables                                                                #OMK:base.omk
# V                .. if set to 1, full command text is shown else short form is used
# W                .. whole tree - if set to 1, make is always called from the top-level directory
# SUBDIRS          .. list of subdirectories intended for make from actual directory
# default_CONFIG   .. list of default config assignments CONFIG_XXX=y/n ...
# LN_HEADERS       .. if "y", header files are symbolicaly linked instead of copied. #OMK:include.omk
# bin_PROGRAMS     .. list of the require binary programs                        #OMK:sdcc.omk
# test_PROGRAMS    .. list of the test programs
# include_HEADERS  .. list of the user-space public header files
# lib_LIBRARIES    .. list of the user-space libraries
# lib_LDSCRIPTS    .. list of LD scripts that should be copied to the lib direcotry
# lib_obj_SOURCES  .. list of source files which should be compiled and
#		      the produced object file placed to the lib directory (e.g. crt0.S)
# shared_LIBRARIES .. list of the user-space shared libraries
# nobase_include_HEADERS .. public headers copied even with directory part
# renamed_include_HEADERS .. public headers copied to the different target name (xxx.h->yyy.h)
# asm_build_HEADERS .. list of files copied to the build object directory
# utils_PROGRAMS   .. list of the development utility programs (compiled for host computer, this might change in future)
# xxx_SOURCES      .. list of specific target sources
# xxx_LIBS         .. list of specific target libraries
# INCLUDES         .. additional include directories and defines for user-space
# lib_LOADLIBES    .. list of libraries linked to each executable
# link_VARIANTS    .. list of ld script suffixes (after hypen `-') that
#                     should be used for linking (e.g. ram flash). If this is not
#		      specified, then the value of DEFAULT_LD_SCRIPT_VARIANT from config.target is used.
# PREFIX_DIR       .. Prefix to  directories in _compiled and _build. Used in config.omk.
# LOCAL_CONFIG_H   .. name of local config.h file generated from values          #OMK:config_h.omk
#                     of options defined in the current directory
# config_include_HEADERS .. names of global config files (possibly
#                     with subdirectories)
# xxx_DEFINES      .. list of config directives to be included in
#                     config header file of the name <somedir>/xxx.h
# DOXYGEN          .. if non-empty, generated headers includes Doxygen's @file
# 		      command, so it is possible to document config
# 		      variables.
# local_EVALUATE   .. Makefile hook, which is executed at the end of             #OMK:localeval.omk
#                     the Makefile.rules. Used only for dirty hacks.
OMK_RULES_TYPE=sdcc                                                              #OMK:Makefile.rules.sdcc@
                                                                                 #OMK:sdcc-defines.omk@Makefile.rules.sdcc
CC = sdcc
EXE_SUFFIX = .ihx
OBJ_EXT = .rel
LIB_EXT = .lib
LIB_PREF =
AR = sdcclib
AS = $(shell if which sdas8051>/dev/null ; then echo sdas8051 ; else echo asx8051 ; fi)
HC = packihx
HEX2BIN = true
OBJCOPY = objcopy
TARGETLOADER = ul_sendhex
ARFLAGS = -a
A51TOASX = $(MAKERULES_DIR)/a51toasx.sh
                                                                                 #OMK:base.omk@Makefile.rules.sdcc
# If we are not called by OMK leaf Makefile...
ifndef MAKERULES_DIR
MAKERULES_DIR := $(abspath $(dir $(filter %Makefile.rules,$(MAKEFILE_LIST))))
endif

# OUTPUT_DIR is the place where _compiled, _build and possible other
# files/directories are created. By default is the same as
# $(MAKERULES_DIR).
ifndef OUTPUT_DIR
OUTPUT_DIR := $(MAKERULES_DIR)
endif

# We need to ensure definition of sources directory first
ifndef SOURCES_DIR
# Only shell built-in pwd understands -L
SOURCES_DIR := $(shell ( pwd -L ) )
INVOCATION_DIR := $(SOURCES_DIR:$(OUTPUT_DIR)%=%)
INVOCATION_DIR := $(INVOCATION_DIR:/%=%)
INVOCATION_DIR := $(INVOCATION_DIR:\\%=%)
endif

.PHONY: all default check-make-ver print-hints omkize

ifdef W
  ifeq ("$(origin W)", "command line")
    OMK_WHOLE_TREE:=$(W)
  endif
endif
ifndef OMK_WHOLE_TREE
  OMK_WHOLE_TREE:=0
endif

ifneq ($(OMK_WHOLE_TREE),1)
all: check-make-ver print-hints default
	@echo "Compilation finished"
else
# Run make in the top-level directory
all:
	@$(MAKE) -C $(MAKERULES_DIR) OMK_SERIALIZE_INCLUDED=n SOURCES_DIR=$(MAKERULES_DIR) RELATIVE_DIR="" $(MAKECMDGOALS) W=0
endif

ifdef OMK_TESTSROOT
# Usage: $(call canttest,<error message>)
define canttest
	( echo "$(1)" > $(OUTPUT_DIR)/_canttest; echo "$(1)"; exit 1 )
endef
else
define canttest
	echo "$(1)"
endef
endif

#=========================
# Include the config file

ifndef CONFIG_FILE
CONFIG_FILE      := $(OUTPUT_DIR)/config.omk
endif

$(CONFIG_FILE)-default:
	$(MAKE) default-config 

ifeq ($(MAKECMDGOALS),default-config)
export DEFAULT_CONFIG_PASS=1
endif

ifneq ($(DEFAULT_CONFIG_PASS),1)
include $(CONFIG_FILE)-default
endif

-include $(OUTPUT_DIR)/config.target

ifneq ($(wildcard $(CONFIG_FILE)),)
-include $(CONFIG_FILE)
endif


CONFIG_FILES ?= $(wildcard $(CONFIG_FILE)-default) $(wildcard $(OUTPUT_DIR)/config.target) $(wildcard $(CONFIG_FILE))


export SOURCES_DIR MAKERULES_DIR RELATIVE_DIR INVOCATION_DIR
export CONFIG_FILE CONFIG_FILES OMK_SERIALIZE_INCLUDED OMK_VERBOSE OMK_SILENT
# OMK_SERIALIZE_INCLUDED has to be exported to submakes because passes
# must to be serialized only in the toplevel make.

ifndef RELATIVE_DIR
RELATIVE_DIR := $(SOURCES_DIR:$(OUTPUT_DIR)%=%)
endif
#$(warning  === RELATIVE_DIR = "$(RELATIVE_DIR)" ===)
override RELATIVE_DIR := $(RELATIVE_DIR:/%=%)
override RELATIVE_DIR := $(RELATIVE_DIR:\\%=%)
#$(warning  RELATIVE_DIR = "$(RELATIVE_DIR)")
#override BACK2TOP_DIR := $(shell echo $(RELATIVE_DIR)/ | sed -e 's_//_/_g' -e 's_/\./_/_g' -e 's_^\./__g'  -e 's_\([^/][^/]*\)_.._g' -e 's_/$$__')
#$(warning  BACK2TOP_DIR = "$(BACK2TOP_DIR)")

#$(warning SOURCES_DIR = "$(SOURCES_DIR)")
#$(warning MAKERULES_DIR = "$(OUTPUT_DIR)")
#$(warning RELATIVE_DIR = "$(RELATIVE_DIR)")

# We have to use RELATIVE_PREFIX because of mingw
override RELATIVE_PREFIX := $(RELATIVE_DIR)/
override RELATIVE_PREFIX := $(RELATIVE_PREFIX:/%=%)

#vpath %.c $(SOURCES_DIR)
#vpath %.cc $(SOURCES_DIR)
#vpath %.cxx $(SOURCES_DIR)

# Define srcdir for Automake compatibility
srcdir = $(SOURCES_DIR)

# Defines for quiet compilation
ifdef V
  ifeq ("$(origin V)", "command line")
    OMK_VERBOSE = $(V)
  endif
endif
ifndef OMK_VERBOSE
  OMK_VERBOSE = 0
endif
ifneq ($(OMK_VERBOSE),0)
  Q =
else
  Q = @
endif
ifneq ($(findstring s,$(MAKEFLAGS)),)
  QUIET_CMD_ECHO = true
  OMK_SILENT = 1
else
  QUIET_CMD_ECHO = echo
endif

MAKEFILE_OMK=Makefile.omk
# All subdirectories (even linked ones) containing Makefile.omk
# Usage in Makefile.omk: SUBDIRS = $(ALL_OMK_SUBDIRS)
ALL_OMK_SUBDIRS = $(patsubst %/$(MAKEFILE_OMK),%,$(patsubst $(SOURCES_DIR)/%,%,$(wildcard $(SOURCES_DIR)/*/$(MAKEFILE_OMK))))

# ===================================================================
# We have set up all important variables, so we can check and include
# real OCERA style Makefile.omk now
ifndef OMK_INCLUDED
include $(SOURCES_DIR)/$(MAKEFILE_OMK)
ifeq ($(AUTOMATIC_SUBDIRS),y)
SUBDIRS?=$(ALL_OMK_SUBDIRS)
endif
OMK_INCLUDED := 1
endif

print-hints:
	@echo 'Use "make V=1" to see the verbose compile lines.'

check-make-ver:
	@GOOD_MAKE_VERSION=`echo $(MAKE_VERSION) | sed -n -e 's/^[4-9]\..*\|^3\.9[0-9].*\|^3\.8[1-9].*/y/p'` ; \
	if [ x$$GOOD_MAKE_VERSION != xy ] ; then \
		echo "Your make program version is too old and does not support OMK system." ; \
		echo "Please update to make program 3.81beta1 or newer." ; exit 1 ; \
	fi

distclean dist-clean:
	@$(QUIET_CMD_ECHO) "  RM      $(COMPILED_DIR_NAME) $(BUILD_DIR_NAME)"
	@rm -fr $(OUTPUT_DIR)/$(COMPILED_DIR_NAME)  $(OUTPUT_DIR)/$(BUILD_DIR_NAME)

# Common OMK templates
# ====================

# Syntax: $(call mkdir,<dir name>)
define mkdir_def
	[ -d $(1) ] || mkdir -p $(1) || exit 1
endef

ifneq ($(OMK_VERBOSE),2)
NO_PRINT_DIRECTORY := --no-print-directory
endif

ifeq ($(USE_LEAF_MAKEFILES),n)
export USE_LEAF_MAKEFILES
SUBDIR_MAKEFILE=$(MAKERULES_DIR)/Makefile.rules
SOURCESDIR_MAKEFILE=$(MAKERULES_DIR)/Makefile.rules
else
SUBDIR_MAKEFILE=$(SOURCES_DIR)/$(3)/Makefile
SOURCESDIR_MAKEFILE=$(SOURCES_DIR)/Makefile
endif

pass = $(strip $(1))

unexport SUBDIRS

# Call a pass in a subdirectory
# Usage: $(call omk_pass_subdir_template,<pass name>,<build dir>,<subdir>)
define omk_pass_subdir_template
.PHONY: $(pass)-$(3)-subdir
$(pass)-submakes: $(pass)-$(3)-subdir
$(pass)-$(3)-subdir: MAKEOVERRIDES:=$(filter-out SUBDIRS=%,$(MAKEOVERRIDES))
$(pass)-$(3)-subdir:
	@$(call mkdir_def,$(2)/$(3))
	+@$(MAKE) SOURCES_DIR=$(SOURCES_DIR)/$(3) $(NO_PRINT_DIRECTORY) \
		RELATIVE_DIR=$(RELATIVE_PREFIX)$(3) -C $(2)/$(3) \
		-f $(SUBDIR_MAKEFILE) $(pass)-submakes
# In subdirectories we can call submakes directly since passes are
# already serialized on the toplevel make.
endef

ifdef OMK_TESTSROOT
check-target = $(1:%=%-check)
endif

# Call a pass in a subdirectory
# Usage: $(call extra_rules_subdir_template,<subdir>)
define extra_rules_subdir_template
extra-rules-subdirs: extra-rules-$(1)
extra-rules-$(1):
	+@$(MAKE) OMK_SERIALIZE_INCLUDED=n MAKERULES_DIR=$(SOURCES_DIR)/$(1) OUTPUT_DIR=$(OUTPUT_DIR) \
		SOURCES_DIR=$(SOURCES_DIR)/$(1) RELATIVE_DIR=$(RELATIVE_PREFIX)$(1) -C $(SOURCES_DIR)/$(1)
endef

.PHONY: extra-rules-subdirs
extra-rules-subdirs:

$(foreach subdir,$(EXTRA_RULES_SUBDIRS),$(eval $(call extra_rules_subdir_template,$(subdir))))

# Usage: $(call omk_pass_template,<pass name>,<build dir>,[<local make flags>],[<local enable condition>])
define omk_pass_template
.PHONY: $(pass) $(pass)-local $(pass)-check $(pass)-submakes
$(foreach subdir,$(SUBDIRS),$(eval $(call omk_pass_subdir_template,$(pass),$(2),$(subdir))))
$(pass):
# Submakes have to be called this way and not as dependecies for pass
# serialization to work
	+@$(MAKE) SOURCES_DIR=$(SOURCES_DIR) $(NO_PRINT_DIRECTORY) \
		RELATIVE_DIR=$(RELATIVE_DIR) \
		-f $(SOURCESDIR_MAKEFILE) $(pass)-submakes
$(pass)-submakes:
	@true			# Do not emit "nothing to be done" messages

ifneq ($(4)$($(pass)_HOOKS),)
$(pass)-submakes: $(pass)-this-dir
$(pass)-this-dir: $(foreach subdir,$(SUBDIRS),$(pass)-$(subdir)-subdir)
	+@echo "make[omk]: $(pass) in $(RELATIVE_DIR)"
	@$(call mkdir_def,$(2))
	+@$(MAKE) $(NO_PRINT_DIRECTORY) SOURCES_DIR=$(SOURCES_DIR) RELATIVE_DIR=$(RELATIVE_DIR) -C $(2) \
	        -f $(SOURCESDIR_MAKEFILE) $(3) $(check-target) $(1:%=%-local)
$(pass)-local: $($(pass)_HOOKS)
endif
endef

# =======================
# DEFAULT CONFIG PASS

default-config:
	@echo "# Start of OMK config file" > "$(CONFIG_FILE)-default"
	@echo "# This file should not be altered manually" >> "$(CONFIG_FILE)-default"
	@echo "# Overrides should be stored in file $(notdir $(CONFIG_FILE))" >> "$(CONFIG_FILE)-default"
	@echo >> "$(CONFIG_FILE)-default"
	@$(MAKE) $(NO_PRINT_DIRECTORY) -C $(OUTPUT_DIR) \
		RELATIVE_DIR="" SOURCES_DIR=$(OUTPUT_DIR) \
		-f $(OUTPUT_DIR)/Makefile default-config-pass

$(eval $(call omk_pass_template,default-config-pass,$$(LOCAL_BUILD_DIR),,always))

default-config-pass-local:
#	@echo Default config for $(RELATIVE_DIR)
	@echo "# Config for $(RELATIVE_DIR)" >> "$(CONFIG_FILE)-default"
	@$(foreach x, $(default_CONFIG), echo '$(x)' | \
		sed -e 's/^[^=]*=x$$/#\0/' >> "$(CONFIG_FILE)-default" ; )


omkize:
	$(Q)if ! grep -q MAKERULES_DIR Makefile; then \
	   echo "Makefile is not OMK leaf makefile!" >&2; exit 1; \
	fi
	$(Q)for i in `find -L . -name Makefile.omk` ; do \
	   d=`dirname $${i}`; \
	   if ! test -f "$${d}/Makefile.rules" && ( test -f "$${d}/Makefile" && ! cmp --silent Makefile "$${d}/Makefile" ); then \
	      rm -f "$${d}/Makefile"; \
	      cp -v Makefile "$${d}/Makefile"; \
	   fi \
	done
ifeq ($(OMK_VERBOSE),1)                                                          #OMK:include.omk@Makefile.rules.sdcc
CPHEADER_FLAGS += -v
LNHEADER_FLAGS += -v
endif

ifneq ($(LN_HEADERS),y)
define cp_cmd
if ! cmp --quiet $(1) $(2); then \
    echo "  CP      $(1:$(OUTPUT_DIR)/%=%) -> $(2:$(OUTPUT_DIR)/%=%)"; \
    install -D $(CPHEADER_FLAGS) $(1) $(2) || exit 1; \
fi
endef
else
define cp_cmd
if ! cmp --quiet $(1) $(2); then \
    echo "  LN      $(1:$(OUTPUT_DIR)/%=%) -> $(2:$(OUTPUT_DIR)/%=%)"; \
    if [ -f $(1) ]; then d=$(2); mkdir -p $${d%/*} && ln -sf $(LNHEADER_FLAGS) $(1) $(2) || exit 1; else exit 1; fi; \
fi
endef
endif

# TODO: Check modification date of changed header files. If it is
# newer that in source dir, show a warning.

# Syntax: $(call include-pass-template,<include dir>,<keyword>)
define include-pass-template
include-pass-local: include-pass-local-$(2)
include-pass-local-$(2): $$($(2)_GEN_HEADERS) $$(foreach f,$$(renamed_$(2)_GEN_HEADERS),$$(shell f='$$(f)'; echo $$$${f%->*}))
	@$$(foreach f, $$($(2)_HEADERS),$$(call cp_cmd,$$(SOURCES_DIR)/$$(f),$(1)/$$(notdir $$(f))); )
# FIXME: Use correct build dir, then document it (in the line bellow)
	@$$(foreach f, $$($(2)_GEN_HEADERS),$$(call cp_cmd,$$(LOCAL_BUILD_DIR)/$$(f),$(1)/$$(notdir $$(f))); )
	@$$(foreach f, $$(nobase_$(2)_HEADERS), $$(call cp_cmd,$$(SOURCES_DIR)/$$(f),$(1)/$$(f)); )
	@$$(foreach f, $$(renamed_$(2)_HEADERS), \
	   f='$$(f)'; srcfname=$$$${f%->*}; destfname=$$$${f#*->}; \
	   $$(call cp_cmd,$$(SOURCES_DIR)/$$$${srcfname},$(1)/$$$${destfname}); )
	@$$(foreach f, $$(renamed_$(2)_GEN_HEADERS), \
	   f='$$(f)'; srcfname=$$$${f%->*}; destfname=$$$${f#*->}; \
	   $$(call cp_cmd,$$(LOCAL_BUILD_DIR)/$$$${srcfname},$(1)/$$$${destfname}); )
# Suppress "Nothing to be done for `include-pass-local'" message if no headers are defined in Makefile.omk
	@$$(if $$($(2)_HEADERS)$$($(2)_GEN_HEADERS)$$(nobase_$(2)_HEADERS)$$(renamed_$(2)_HEADERS)$$(renamed_$(2)_GEN_HEADERS),,true)
endef
                                                                                 #OMK:sdcc.omk@Makefile.rules.sdcc
BUILD_DIR_NAME = _build
COMPILED_DIR_NAME = _compiled
GROUP_DIR_NAME =

BUILD_DIR_NAME = _build$(addprefix /,$(PREFIX_DIR))
COMPILED_DIR_NAME = _compiled$(addprefix /,$(PREFIX_DIR))

LOCAL_BUILD_DIR=$(MAKERULES_DIR)/$(BUILD_DIR_NAME)/$(RELATIVE_DIR)
#$(warning LOCAL_BUILD_DIR = $(LOCAL_BUILD_DIR))

#=====================================================================
# Common utility rules

link_VARIANTS ?= $(DEFAULT_LD_SCRIPT_VARIANT)

#=====================================================================
# Include correct rules for just running pass

USER_COMPILED_DIR_NAME=$(MAKERULES_DIR)/$(COMPILED_DIR_NAME)

USER_INCLUDE_DIR = $(USER_COMPILED_DIR_NAME)/include
USER_LIB_DIR     = $(USER_COMPILED_DIR_NAME)/lib
USER_UTILS_DIR   = $(USER_COMPILED_DIR_NAME)/bin-utils
USER_TESTS_DIR   = $(USER_COMPILED_DIR_NAME)/bin-tests
USER_BIN_DIR     = $(USER_COMPILED_DIR_NAME)/bin
USER_OBJS_DIR    = $(LOCAL_BUILD_DIR)

.PHONY: check-dir

# Some support to serialize some targets for parallel make
ifneq ($(OMK_SERIALIZE_INCLUDED),y)
include-pass: check-dir
library-pass: include-pass
binary-pass utils-pass: library-pass

override OMK_SERIALIZE_INCLUDED = y
MAKEOVERRIDES := $(filter-out OMK_SERIALIZE_INCLUDED=n,$(MAKEOVERRIDES))
endif

CPPFLAGS  += -I $(USER_INCLUDE_DIR)

CPPFLAGS  += $(CONFIG_OMK_DEFINES)

CFLAGS  += $(TARGET_ARCH) 
LDFLAGS += $(TARGET_ARCH) 

LOADLIBES += -L$(USER_LIB_DIR) 

LOADLIBES += $(lib_LOADLIBES:%=-l%)

INCLUDE_DIR := $(USER_INCLUDE_DIR)
LIB_DIR     := $(USER_LIB_DIR)
OBJS_DIR    := $(USER_OBJS_DIR)

# Checks for OMK tester
ifdef OMK_TESTSROOT
default-config-pass-check include-pass-check:
library-pass-check binary-pass-check:
	@[ -x "$(shell which $(CC))" ] || $(call canttest,Cannot find compiler: $(CC))
	@[ -z "$(wildcard $(USER_LIB_DIR)/$(LD_SCRIPT).ld*)" ] && $(call canttest,LD_SCRIPT $(LD_SCRIPT).ld* not found)
endif

#=====================================================================
# User-space rules and templates to compile programs, libraries etc.

ifdef USER_RULE_TEMPLATES

USER_SOURCES2OBJS = $(OBJ_EXT)/.c $(OBJ_EXT)/.cc $(OBJ_EXT)/.cxx $(OBJ_EXT)/.S $(OBJ_EXT)/.asm $(OBJ_EXT)/.o

#%.o: %.c
#	$(CC) -o $@ $(LCFLAGS) -c $<

c_o_COMPILE = $(CC) $(DEFS) $(DEFAULT_INCLUDES) $(AM_CPPFLAGS) \
	$(CPPFLAGS) $(AM_CFLAGS) $(CFLAGS) $(INCLUDES) -DOMK_FOR_USER

cc_o_COMPILE = $(CXX) $(DEFS) $(DEFAULT_INCLUDES) $(AM_CPPFLAGS) \
	$(CPPFLAGS) $(AM_CXXFLAGS) $(CFLAGS) $(INCLUDES) -DOMK_FOR_USER

S_o_COMPILE = $$(CC) $(DEFS) $(DEFAULT_INCLUDES) $(INCLUDES) \
	$(CPPFLAGS) $(AM_CFLAGS) $$(CFLAGS) $(ASFLAGS)

# Check C compiler version for user build
ifndef CC_MAJOR_VERSION
CC_MAJOR_VERSION := sdcc
endif


# Syntax: $(call COMPILE_c_o_template,<source>,<target>,<additional c-flags>)
define COMPILE_c_o_template
$(2): $(1) $$(GEN_HEADERS)
	@$(QUIET_CMD_ECHO) "  CC      $$@"
	$(Q) if $$(c_o_COMPILE) $(3) -o "$$@" -c $$< ; \
	then $$(c_o_COMPILE) $(3) -M $$< > "$$@.d" ; \
	else exit 1; \
	fi
endef


# Syntax: $(call COMPILE_cc_o_template,<source>,<target>,<additional c-flags>)
define COMPILE_cc_o_template
$(2): $(1) $(LOCAL_CONFIG_H)
	@$(QUIET_CMD_ECHO) "  CXX     $$@"
	$(Q) echo "C++ compilation not suported for this compiler"
endef


# Syntax: $(call COMPILE_S_o_template,<source>,<target>,<additional c-flags>)
define COMPILE_S_o_template
$(2): $(1) $$(GEN_HEADERS)
	@$(QUIET_CMD_ECHO) "  CC      $$@"
	$(Q) if $$(S_o_COMPILE) $(3) -o "$$@" -c $$< ; \
	then $$(c_o_COMPILE) $(3) -M $$< > "$$@.d" ; \
	else exit 1; \
	fi
endef

# Syntax: $(call COMPILE_asm_o_template,<source>,<target>,<additional c-flags>)
define COMPILE_asm_o_template
$(2): $(1) $(LOCAL_CONFIG_H)
	@$(QUIET_CMD_ECHO) "  ASM      $$@"
	$(Q) $$(A51TOASX) $(1) $(2:%$(OBJ_EXT)=%.s)
	$(Q) $$(AS) -l -o $(2:%$(OBJ_EXT)=%.s)
endef


# Syntax: $(call PROGRAM_template,<executable-name>,<dir>,<executable-suffix>,<linker-sript>)
define PROGRAM_template

$(foreach x, $(USER_SOURCES2OBJS),
$(1)_OBJS += $$(patsubst %$(notdir $(x)),%$(dir $(x)),$$(filter %$(notdir $(x)),\
		$$($(1)_SOURCES) $$($(1)_GEN_SOURCES)))
)
$(1)_OBJS := $$(sort $$($(1)_OBJS:%/=%))

USER_OBJS  += $$($(1)_OBJS)
USER_SOURCES += $$($(1)_SOURCES)

-include $(USER_LIB_DIR)/$(LD_SCRIPT).ld$$(addprefix -,$(4))

$(2)/$(1)$(addprefix -,$(4))$(3): $(USER_LIB_DIR)/$(LD_SCRIPT).ld$$(addprefix -,$(4))

$(2)/$(1)$(addprefix -,$(4))$(3): $(USER_LIB_DIR)/timestamp

$(2)/$(1)$(addprefix -,$(4))$(3): $$($(1)_OBJS)
	@$(QUIET_CMD_ECHO) "  LINK    $$@"
	$(Q) $$(shell if [ -z "$$(filter %.cc,$$($(1)_SOURCES))" ] ; \
	  then echo $$(CC)  $$(CPPFLAGS) $$(AM_CPPFLAGS) $$(AM_CFLAGS) $$(CFLAGS) ; \
	  else echo $$(CXX) $$(CPPFLAGS) $$(AM_CPPFLAGS) $$(AM_CXXFLAGS) $$(CXXFLAGS) ; fi) \
	  $$(AM_LDFLAGS) $$(LDFLAGS) $$($(1)_OBJS) $$(LOADLIBES) $$($(1)_LIBS:%=-l%) \
	  -o $(2)/$(1)$(addprefix -,$(4))$(3)
	$(HEX2BIN) $(2)/$(1)$(addprefix -,$(4))$(3)
endef

# Rules for other output formats (can be specified by OUTPUT_FORMATS)
%.bin: %$(EXE_SUFFIX)
	@$(QUIET_CMD_ECHO) "  OBJCOPY $@"
	$(Q) $(OBJCOPY) --input-target=ihex --output-target=binary $< $@

%.hex: %$(EXE_SUFFIX)
	@$(QUIET_CMD_ECHO) "  GENIHEX $@"
	$(Q) cd $(dirname $<) && $(HC) $< >$@

# Syntax: $(call LIBRARY_template,<library-name>)
define LIBRARY_template
$(foreach x, $(USER_SOURCES2OBJS),
$(1)_OBJS += $$(patsubst %$(notdir $(x)),%$(dir $(x)),$$(filter %$(notdir $(x)),\
		$$($(1)_SOURCES) $$($(1)_GEN_SOURCES)))
)
$(1)_OBJS := $$(sort $$($(1)_OBJS:%/=%))

USER_OBJS  += $$($(1)_OBJS)
USER_SOURCES += $$($(1)_SOURCES)

$(USER_LIB_DIR)/$(LIB_PREF)$(1)$(LIB_EXT): $$($(1)_OBJS)
	@$(QUIET_CMD_ECHO) "  AR      $$@"
	$(Q) for i in $$^ ; do $(AR) $(ARFLAGS) $$@ $$$$i ; done
	@touch $(USER_LIB_DIR)/timestamp
endef

# -------------------------------------
# Rules for compilation for target

# Generate rules for compilation of programs and libraries
ifneq ($(link_VARIANTS),)
$(foreach prog,$(bin_PROGRAMS),$(foreach link,$(link_VARIANTS),$(eval $(call PROGRAM_template,$(prog),$(USER_BIN_DIR),$(EXE_SUFFIX),$(link)))))
$(foreach prog,$(utils_PROGRAMS),$(foreach link,$(link_VARIANTS),$(eval $(call PROGRAM_template,$(prog),$(USER_UTILS_DIR),$(EXE_SUFFIX),$(link)))))
$(foreach prog,$(test_PROGRAMS),$(foreach link,$(link_VARIANTS),$(eval $(call PROGRAM_template,$(prog),$(USER_TESTS_DIR),$(EXE_SUFFIX),$(link)))))
else
$(foreach prog,$(bin_PROGRAMS),$(eval $(call PROGRAM_template,$(prog),$(USER_BIN_DIR),$(EXE_SUFFIX))))
$(foreach prog,$(utils_PROGRAMS),$(eval $(call PROGRAM_template,$(prog),$(USER_UTILS_DIR),$(EXE_SUFFIX))))
$(foreach prog,$(test_PROGRAMS),$(eval $(call PROGRAM_template,$(prog),$(USER_TESTS_DIR),$(EXE_SUFFIX))))
endif

$(foreach lib,$(lib_LIBRARIES),$(eval $(call LIBRARY_template,$(lib))))
$(foreach src,$(lib_obj_SOURCES),$(eval $(call LIBOBJ_template,$(addsuffix $(OBJ_EXT),$(basename $(src))))))

# lib_obj_SOURCES handling
lib_OBJS = $(addsuffix $(OBJ_EXT),$(basename $(lib_obj_SOURCES)))
#$(warning lib_OBJS = $(lib_OBJS))
SOURCES += $(filter-out %$(OBJ_EXT),$(lib_obj_SOURCES))

$(LIB_DIR)/%$(OBJ_EXT): %$(OBJ_EXT)
	@echo "  CP      $(^:$(MAKERULES_DIR)/%=%) -> $(@:$(MAKERULES_DIR)/%=%)"
	$(Q)cp $(CP_FLAGS) $< $@


# User-space static libraries and applications object files
USER_SOURCES := $(sort $(USER_SOURCES))
#$(warning USER_SOURCES = $(USER_SOURCES))


# The above generated rules produced $(USER_SOURCES) and $(SOLIB_SOURCES)
# variables. Now generate rules for compilation of theese USER_SOURCES
$(foreach src,$(filter %.c,$(USER_SOURCES)),$(eval $(call COMPILE_c_o_template,$(SOURCES_DIR)/$(src),$(src:%.c=%$(OBJ_EXT)),)))
$(foreach src,$(filter %.cc,$(USER_SOURCES)),$(eval $(call COMPILE_cc_o_template,$(SOURCES_DIR)/$(src),$(src:%.cc=%$(OBJ_EXT)),)))
$(foreach src,$(filter %.cxx,$(USER_SOURCES)),$(eval $(call COMPILE_cc_o_template,$(SOURCES_DIR)/$(src),$(src:%.cxx=%$(OBJ_EXT)),)))
$(foreach src,$(filter %.S,$(USER_SOURCES)),$(eval $(call COMPILE_S_o_template,$(SOURCES_DIR)/$(src),$(src:%.S=%$(OBJ_EXT)),)))
$(foreach src,$(filter %.asm,$(USER_SOURCES)), $(eval $(call COMPILE_asm_o_template,$(SOURCES_DIR)/$(src),$(src:%.asm=%$(OBJ_EXT)),)))


library-pass-local: $(addprefix $(USER_INCLUDE_DIR)/,$(cmetric_include_HEADERS)) \
		    $(lib_LIBRARIES:%=$(LIB_DIR)/$(LIB_PREF)%$(LIB_EXT)) $(shared_LIBRARIES:%=$(LIB_DIR)/$(LIB_PREF)%.so) \
		    $(addprefix $(LIB_DIR)/,$(lib_OBJS))

ifneq ($(link_VARIANTS),)
binary-pass-local:  $(foreach link,$(link_VARIANTS),$(bin_PROGRAMS:%=$(USER_BIN_DIR)/%-$(link)$(EXE_SUFFIX)) \
		    $(foreach of,$(OUTPUT_FORMATS),$(bin_PROGRAMS:%=$(USER_BIN_DIR)/%-$(link).$(of))))
binary-pass-local:  $(foreach link,$(link_VARIANTS),$(test_PROGRAMS:%=$(USER_TESTS_DIR)/%-$(link)$(EXE_SUFFIX)) \
		    $(foreach of,$(OUTPUT_FORMATS),$(test_PROGRAMS:%=$(USER_TESTS_DIR)/%-$(link).$(of))))
else
binary-pass-local:  $(bin_PROGRAMS:%=$(USER_BIN_DIR)/%$(EXE_SUFFIX)) \
		    $(foreach of,$(OUTPUT_FORMATS),$(bin_PROGRAMS:%=$(USER_BIN_DIR)/%.$(of)))
binary-pass-local:  $(test_PROGRAMS:%=$(USER_TESTS_DIR)/%$(EXE_SUFFIX)) \
		    $(foreach of,$(OUTPUT_FORMATS),$(test_PROGRAMS:%=$(USER_TESTS_DIR)/%.$(of)))
endif


library-pass-local: $(lib_LIBRARIES:%=$(USER_LIB_DIR)/$(LIB_PREF)%$(LIB_EXT)) $(shared_LIBRARIES:%=$(USER_LIB_DIR)/$(LIB_PREF)%.so)

utils-pass-local: $(foreach link,$(link_VARIANTS),$(utils_PROGRAMS:%=$(USER_UTILS_DIR)/%-$(link)) $(foreach of,$(OUTPUT_FORMATS),$(utils_PROGRAMS:%=$(USER_UTILS_DIR)/%-$(link).$(of))))

-include $(USER_OBJS_DIR)/*.d

endif

#=====================================================================
# Automatic loading of compiled program by issuing "make load"

ifneq ($(OUTPUT_FORMATS),)
# Select a file extension (e.g. .bin) for "make load" command to load.
LOAD_EXTENSION = .$(firstword $(OUTPUT_FORMATS))
endif

# Syntax: $(call LOAD_PROGRAM_template,<executable-name>,<dir>,<link-variant>)
# Used to load program to the target hardware
define LOAD_PROGRAM_template
.PHONY: load-$(1)$(addprefix -,$(3))
-include $(USER_LIB_DIR)/$(LD_SCRIPT).ld$$(addprefix -,$(3))
load-$(1)$(addprefix -,$(3)): $(2)/$(1)$(addprefix -,$(3))$(LOAD_EXTENSION)
	@$(QUIET_CMD_ECHO) "  LOAD    $$<"
	@if [ -z "$$(LOAD_CMD$(addprefix -,$(3)))" ]; then echo "No command for loading applications to '$(3)' is specified."; exit 1; fi
	$(Q) $$(LOAD_CMD$(addprefix -,$(3))) $$<
endef

# Syntax: $(call LOAD__RUN_VARIANT_template,<link-variant>)
# Used to load and/or run non-default variant of the default program
define LOAD_RUN_VARIANT_template
.PHONY: load-$(1) run-$(1)
-include $(USER_LIB_DIR)/$(LD_SCRIPT).ld$$(addprefix -,$(3))
load-$(1): load-$(firstword $(bin_PROGRAMS))-$(1)
run-$(1):
	@$(QUIET_CMD_ECHO) "  RUN     $(1)"
	@if [ -z "$(RUN_CMD-$(1))" ]; then echo "No command for running '$(1)' variant is specified."; exit 1; fi
	$(Q) $(RUN_CMD-$(1))
endef

$(foreach link,$(link_VARIANTS),$(foreach prog,$(bin_PROGRAMS) $(test_PROGRAMS),$(eval $(call LOAD_PROGRAM_template,$(prog),$(USER_BIN_DIR),$(link)))))
$(foreach link,$(link_VARIANTS),$(eval $(call LOAD_RUN_VARIANT_template,$(link))))

.PHONY: load run
load: $(addprefix load-,$(firstword $(bin_PROGRAMS) $(test_PROGRAMS))-$(firstword $(link_VARIANTS)))

run: run-$(firstword $(link_VARIANTS))

#=====================================================================
# Generate pass rules from generic templates

$(eval $(call omk_pass_template, include-pass, $(LOCAL_BUILD_DIR),,$(include_HEADERS)$(nobase_include_HEADERS)$(renamed_include_HEADERS)$(lib_LDSCRIPTS)$(config_include_HEADERS)$(LOCAL_CONFIG_H)))
$(eval $(call omk_pass_template, library-pass, $(LOCAL_BUILD_DIR),USER_RULE_TEMPLATES=y,$(lib_LIBRARIES)$(shared_LIBRARIES)$(lib_obj_SOURCES)))
$(eval $(call omk_pass_template, binary-pass,  $(LOCAL_BUILD_DIR),USER_RULE_TEMPLATES=y,$(bin_PROGRAMS)$(test_PROGRAMS)))
$(eval $(call omk_pass_template, utils-pass,   $(LOCAL_BUILD_DIR),USER_RULE_TEMPLATES=y,$(utils_PROGRAMS)))
$(eval $(call omk_pass_template, dep,    $(LOCAL_BUILD_DIR),,always))
$(eval $(call omk_pass_template, clean,  $(LOCAL_BUILD_DIR),,always))
$(eval $(call omk_pass_template, install,$(LOCAL_BUILD_DIR),,always))


dep-local:

install-local:

$(eval $(call include-pass-template,$(USER_INCLUDE_DIR),include))

include-pass-local:
	@$(foreach f, $(lib_LDSCRIPTS), cmp --quiet $(SOURCES_DIR)/$(f) $(USER_LIB_DIR)/$(notdir $(f)) \
	   || $(call cp_cmd,$(SOURCES_DIR)/$(f),$(USER_LIB_DIR)/$(notdir $(f))) || exit 1 ; )
	@$(foreach f, $(asm_build_HEADERS), \
	   srcfname=`echo '$(f)' | sed -e 's/^\(.*\)->.*$$/\1/'` ; destfname=`echo '$(f)' | sed -e 's/^.*->\(.*\)$$/\1/'` ; \
	   cmp --quiet $(SOURCES_DIR)/$${srcfname} $(USER_OBJS_DIR)/$${destfname} \
	   || ( mkdir -p `dirname $(USER_OBJS_DIR)/$${destfname}` && \
	   $(A51TOASX) $(SOURCES_DIR)/$${srcfname} $(USER_OBJS_DIR)/$${destfname} ) || exit 1 ; )


.PHONY: clean-custom
clean-local: clean-custom
	@echo Cleaning in $(USER_OBJS_DIR)
	$(Q)rm -f $(USER_OBJS_DIR)/*$(OBJ_EXT) $(USER_OBJS_DIR)/*.lo \
	       $(USER_OBJS_DIR)/*.d \
	       $(USER_OBJS_DIR)/*.map \
	       $(LOCAL_CONFIG_H:%=$(USER_OBJS_DIR)/%) \
	       $(tar_EMBEDFILES:%=$(USER_OBJS_DIR)/%_tarfile)
	$(Q)$(foreach confh,$(addprefix $(USER_INCLUDE_DIR)/,$(config_include_HEADERS)),\
	    if [ -e $(confh) ] ; then touch -t 200001010101 $(confh) ; fi ; \
	)

check-dir::
	@$(call mkdir_def,$(USER_OBJS_DIR))
	@$(call mkdir_def,$(USER_INCLUDE_DIR))
	@$(call mkdir_def,$(USER_LIB_DIR))
	@$(call mkdir_def,$(USER_BIN_DIR))
	@$(call mkdir_def,$(USER_UTILS_DIR))
	@$(call mkdir_def,$(USER_TESTS_DIR))

include-pass-submakes: extra-rules-subdirs
# Which passes to pass
default: include-pass library-pass binary-pass utils-pass
                                                                                 #OMK:config_h.omk@Makefile.rules.sdcc
# Syntax: $(call BUILD_CONFIG_H_template,<stamp_dir>,<header_file_path>,<list_of_options_to_export>,<header_barrier>)
define BUILD_CONFIG_H_template

$(addprefix $(1)/,$(notdir $(addsuffix .stamp,$(2)))) : $(CONFIG_FILES)
	@$(QUIET_CMD_ECHO) "  CONFGEN $(notdir $(2))"
	@if [ ! -d `dirname $(2).tmp` ] ; then \
		mkdir -p `dirname $(2).tmp` ; fi
	@echo "/* Automatically generated from */" > "$(2).tmp"
	@echo "/* config files: $$(^:$(OUTPUT_DIR)/%=%) */" >> "$(2).tmp"
	$(if $(DOXYGEN),@echo "/** @file */" >> "$(2).tmp")
	@echo "#ifndef $(4)" >> "$(2).tmp"
	@echo "#define $(4)" >> "$(2).tmp"
	@( $(foreach x, $(shell echo '$($(3))' | tr 'x\t ' 'x\n\n' | sed -e 's/^\([^ =]*\)\(=[^ ]\+\|\)$$/\1/' ), \
		echo '$(x).$($(x))' ; ) echo ; ) | \
		sed -e '/^[^.]*\.n$$$$/d' -e '/^[^.]*\.$$$$/d' -e 's/^\([^.]*\)\.[ym]$$$$/\1.1/' | \
		sed -n -e 's/^\([^.]*\)\.\(.*\)$$$$/#define \1 \2/p' \
		  >> "$(2).tmp"
	@echo "#endif /*$(4)*/" >> "$(2).tmp"
	@touch "$$@"
	@if cmp --quiet "$(2).tmp" "$(2)" ; then rm "$(2).tmp"; \
	else mv "$(2).tmp" "$(2)" ; \
	echo "Updated configuration $(2)" ; fi

endef

ifdef LOCAL_CONFIG_H

# This must be declared after the default cflags are assigned!
# Override is used to override command line assignemnt.
override CFLAGS += -I $(USER_OBJS_DIR)
override kernel_INCLUDES += -I $(KERN_OBJS_DIR)
$(eval $(call BUILD_CONFIG_H_template,$(USER_OBJS_DIR),$(USER_OBJS_DIR)/$(LOCAL_CONFIG_H),default_CONFIG,_LOCAL_CONFIG_H) )

endif

# Special rules for configuration exported headers

#FIXME: The directory for headers should not be specified here.
$(foreach confh,$(config_include_HEADERS),$(eval $(call BUILD_CONFIG_H_template,$(USER_OBJS_DIR),$(addprefix $(USER_INCLUDE_DIR)/,$(confh)),$(basename $(notdir $(confh)))_DEFINES,\
_$(basename $(notdir $(confh)))_H \
)))

config_h_stamp_files = $(addprefix $(USER_OBJS_DIR)/,$(notdir $(addsuffix .stamp,$(config_include_HEADERS) $(LOCAL_CONFIG_H))))

# Add some hooks to standard passes
include-pass-local: $(config_h_stamp_files)

ifneq ($(KERN_CONFIG_HEADERS_REQUIRED),)

ifdef LOCAL_CONFIG_H
$(eval $(call BUILD_CONFIG_H_template,$(KERN_OBJS_DIR),$(KERN_OBJS_DIR)/$(LOCAL_CONFIG_H),default_CONFIG,_LOCAL_CONFIG_H) )
endif

$(foreach confh,$(config_include_HEADERS),$(eval $(call BUILD_CONFIG_H_template,$(KERN_OBJS_DIR),$(addprefix $(KERN_INCLUDE_DIR)/,$(confh)),$(basename $(notdir $(confh)))_DEFINES,\
_$(basename $(notdir $(confh)))_H \
)))

kern_config_h_stamp_files = $(addprefix $(KERN_OBJS_DIR)/,$(notdir $(addsuffix .stamp,$(config_include_HEADERS) $(LOCAL_CONFIG_H))))

# Add some hooks to standard passes
include-pass-local: $(kern_config_h_stamp_files)

endif

clean-local: clean-local-config-h

clean-local-config-h:
	@$(foreach confh,$(config_h_stamp_files) $(kern_config_h_stamp_files),\
	    if [ -e $(confh) ] ; then rm $(confh) ; fi ; \
	)
                                                                                 #OMK:sources-list.omk@Makefile.rules.sdcc
# Rules that creates the list of files which are used during
# compilation. The list reflects conditional compilation depending on
# config.omk and other variables.

SOURCES_LIST_FN=sources.txt
ifndef SOURCES_LIST
SOURCES_LIST_DIR:=$(RELATIVE_DIR)
SOURCES_LIST:=$(OUTPUT_DIR)/$(SOURCES_LIST_DIR)/$(SOURCES_LIST_FN)
SOURCES_LIST_D := $(LOCAL_BUILD_DIR)/$(SOURCES_LIST_FN).d
export SOURCES_LIST SOURCES_LIST_DIR SOURCES_LIST_D
endif

ifneq ($(filter sources-list TAGS tags cscope,$(MAKECMDGOALS)),)
NEED_SOURCES_LIST=y
endif

ifeq ($(NEED_SOURCES_LIST),y) # avoid execution of find command bellow if it is not useful
.PHONY: sources-list
sources-list: $(SOURCES_LIST)

$(SOURCES_LIST): $(CONFIG_FILES) $(shell find -name $(MAKEFILE_OMK))
	@$(call mkdir_def,$(dir $(SOURCES_LIST_D)))
	@echo -n "" > "$(SOURCES_LIST).tmp"
	@echo -n "" > "$(SOURCES_LIST_D).tmp"
	@$(MAKE) --no-print-directory sources-list-pass
	@echo "# Automatically generated list of files in '$(RELATIVE_DIR)' that are used during OMK compilation" > "$(SOURCES_LIST).tmp2"
	@cat "$(SOURCES_LIST).tmp"|sort|uniq >> "$(SOURCES_LIST).tmp2"
	@rm "$(SOURCES_LIST).tmp"
	@mv "$(SOURCES_LIST).tmp2" "$(SOURCES_LIST)"
	@echo "$(SOURCES_LIST): \\" > "$(SOURCES_LIST_D).tmp2"
	@cat "$(SOURCES_LIST_D).tmp"|grep -v "$(SOURCES_LIST_D).tmp"|sort|uniq|\
		sed -e 's/$$/\\/' >> "$(SOURCES_LIST_D).tmp2"
	@rm "$(SOURCES_LIST_D).tmp"
	@mv "$(SOURCES_LIST_D).tmp2" "$(SOURCES_LIST_D)"
endif

$(eval $(call omk_pass_template,sources-list-pass,$$(LOCAL_BUILD_DIR),,always))

sources-list-pass-local:
	@$(foreach m,$(MAKEFILE_LIST),echo '  $(m)' >> "$(SOURCES_LIST_D).tmp";)
	@$(foreach h,$(include_HEADERS) $(nobase_include_HEADERS) $(kernel_HEADERS),\
	  echo "$(addsuffix /,$(RELATIVE_DIR:$(SOURCES_LIST_DIR)/%=%))$(h)" >> "$(SOURCES_LIST).tmp";)
	@$(foreach ch,$(config_include_HEADERS), \
	  echo "$(USER_INCLUDE_DIR:$(OUTPUT_DIR)/$(addsuffix /,$(SOURCES_LIST_DIR))%=%)/$(ch)" >> "$(SOURCES_LIST).tmp";)
	@$(foreach h,$(renamed_include_HEADERS),echo '$(h)'|sed -e 's|\(.*\)->.*|$(addsuffix /,$(RELATIVE_DIR:$(SOURCES_LIST_DIR)/%=%))\1|' >> "$(SOURCES_LIST).tmp";)
	@$(foreach bin,$(lib_LIBRARIES) $(shared_LIBRARIES) $(bin_PROGRAMS) $(test_PROGRAMS) $(utils_PROGRAMS) \
	  $(kernel_LIBRARIES) $(rtlinux_LIBRARIES) $(kernel_MODULES),\
	  $(foreach src,$(filter-out %.o,$($(bin)_SOURCES)),echo "$(addsuffix /,$(RELATIVE_DIR:$(SOURCES_LIST_DIR)/%=%))$(src)" >> "$(SOURCES_LIST).tmp";))

############ TAGS ###########

ifeq ($(MAKECMDGOALS),TAGS)
ETAGS=etags
TAGS_CMD = $(ETAGS)
TAGS: $(SOURCES_LIST)
	@$(MAKE) --no-print-directory do-tags
endif
ifeq ($(MAKECMDGOALS),tags) 
CTAGS=ctags -N
TAGS_CMD = $(CTAGS)
tags: $(SOURCES_LIST)
	@$(MAKE) --no-print-directory do-tags
endif
export TAGS_CMD

ifeq ($(MAKECMDGOALS),do-tags)
.PHONY: do-tags
do-tags: $(shell sed -e '/^\#/d' $(SOURCES_LIST))
	@$(QUIET_CMD_ECHO) "  TAGS    $(SOURCES_LIST_FN)"
	$(Q)$(TAGS_CMD) $^
endif

############ CSCOPE ###########

cscope: $(SOURCES_LIST)
	@$(QUIET_CMD_ECHO) "  CSCOPE  < $(SOURCES_LIST_FN)"
	$(Q)sed -e '/^#/d' $(SOURCES_LIST) > cscope.files
	$(Q)cscope -b -icscope.files
#FIXME: see doc to -i in cscope(1)
                                                                                 #OMK:localeval.omk@Makefile.rules.sdcc
ifneq ($(local_EVALUATE),)
#$(warning $(local_EVALUATE))
$(eval $(local_EVALUATE))
endif
