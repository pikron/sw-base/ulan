#!/bin/bash

FILENAME=ulan

if [ $# -gt 0 ] ; then
  FILENAME=`basename "$1" .asm`
fi
FILENAMEIN="${FILENAME}.asm"
FILENAMEOUT="${FILENAME}.s"

if [ $# -gt 1 ] ; then
  FILENAMEIN="$1"
  FILENAMEOUT="$2"
fi

# SDCC : mcs51/gbz80/z80/avr/ds390/pic16/pic14/TININative/xa51/ds400/hc08 2.5.1

SDCC_VERSION="$(sdcc --version 2>&1 | sed -n -e 's/SDCC *: *[^ ]* *\<\([0-9]*\.[0-9]*\.[0-9]*\)\>.*$/\1/p')"

case "${SDCC_VERSION}" in
  1.*.* | 2.4.* | 2.5.1 | 2.5.2 ) SDCC_IS_OLD=yes ;;
esac

if [ -n "${SDCC_IS_OLD}" ] ; then
  exec $0-old "$@"
fi

if true ; then
cat ${FILENAMEIN} | strip0d | \
tr '[A-Z]' '[a-z]' | \
sed -e 's/^[[:blank:]]*\$include[[:blank:]]*(\(.*\))/.include "\1"/g' \
 -e 's/^[[:blank:]]*%define[[:blank:]]*(\(.*\))[[:blank:]]*(\(.*\))/\1 = \2/g' \
 -e 's/[[:blank:]]*%if[[:blank:]]*([[:blank:]]*not[[:blank:]]*\(.*\))[[:blank:]]*then[[:blank:]]*(/.if 1^\1/g' \
 -e 's/[[:blank:]]*%if[[:blank:]]*(\(.*\))[[:blank:]]*then[[:blank:]]*(/.if \1/g' \
 -e 's/[[:blank:]]*)[[:blank:]]*fi/.endif /g' \
 -e 's/[[:blank:]]*)[[:blank:]]*else[[:blank:]]*(/.else /g' \
 -e 's/\(..*\)\(.endif\|.else\)/\1\
\2/g' \
 -e 's/\<equ\>/=/g' \
 -e "s/\<db[[:blank:]]*'\([^']*\)',0\>/.asciz '\1'/g" \
 -e 's/\<\(db\|ds\|dw\)\>/.\1/g' \
 -e 's/\<public\>/.globl/g' \
 -e 's/\<extrn\>[[:blank:]]*\([[:alpha:]]*\)[[:blank:]]*(\(.*\))/.globl \2 ; \1/g' \
 -e 's/%//g' \
 -e 's/\<\([0-9][0-9A-Fa-f]*\)[Hh]\>/0x\1/g' \
 -e 's/\<\([0-1][0-1]*\)[Bb]\>/0b\1/g' \
 -e 's/^[^;]*\<segment\>/;\0/g' \
 -e 's/^\([^;]*\)\<call\>/\1lcall/g' \
 -e 's/^\([^;]*\)\<jmp\>/\1ljmp/g' \
 -e 's/^\([^;]*\)\<end\>/\1/g' \
 -e 's/^\([^;]*\)\<low\>/\1</g' \
 -e 's/^\([^;]*\)\<high\>/\1>/g' \
 -e 's/^\([^;]*\)\<and\>/\1\&/g' \
 -e 's/^\([^;]*\)\<or\>/\1|/g' \
 -e 's/^\([^;]*\)\<xor\>/\1^/g' \
 -e 's/^\([^;]*\)\<not\>/\1~/g' \
 -e 's/^[[:blank:]]*rseg[[:blank:]]*\([[:alnum:]_]*_c\)/.area CSEG (CODE) ;\1/g' \
 -e 's/^[[:blank:]]*rseg[[:blank:]]*\([[:alnum:]_]*_x\)/.area XSEG (XDATA);\1/g' \
 -e 's/^[[:blank:]]*rseg[[:blank:]]*\([[:alnum:]_]*_d\)/.area DSEG (DATA) ;\1/g' \
 -e 's/^[[:blank:]]*rseg[[:blank:]]*\([[:alnum:]_]*_i\)/.area ISEG (DATA) ;\1/g' \
\
 -e 's/^[[:blank:]]*rseg[[:blank:]]*\([[:alnum:]_]*_b\)/.area BSEG_BYTES (DATA) ;\1/g' \
 -e 's/^[[:blank:]]*rseg[[:blank:]]*\([[:alnum:]_]*_rb1\)/.area REG_BANK_1 (REL,OVR,DATA) ;\1/g' \
 -e 's/^[[:blank:]]*rseg[[:blank:]]*\(ser_stack\)/.area DSEG (DATA) \
\1:/g' \
 -e 's/^[[:blank:]]*\([[:alnum:]_]*\)[[:blank:]]*bit[[:blank:]]*\([[:alnum:]_]*\)\.\([0-7]\)/\1=\2[\3] /g' \
 -e 's/^[[:blank:]]*\([[:alnum:]_]*\)[[:blank:]]*bit[[:blank:]]*\([[:alnum:]_.]*\)/\1=\2 /g' \
\
 -e 's/^[[:blank:]]*rseg[[:blank:]]*\(ser_stack\)/.area DSEG (DATA) \
\1:/g' \
 -e 's/^[[:blank:]]*rseg[[:blank:]]*\([[:alnum:]_]*\)/.area \1/g' \
 -e 's/^[[:blank:]]*\([[:alnum:]_]*\)[[:blank:]]*\(xdata\|data\|idata\|code\)[[:blank:]]*\([[:alnum:]_]*\)/\1=\3 ;\2 /g' \
 -e 's/\<cseg[[:blank:]]*at\>/.area CSEG_A (ABS,CODE)\
.org/g' \
 -e 's/\<xseg[[:blank:]]*at\>/.area XSEG_A (ABS,XDATA)\
.org/g' \
 -e 's/\<dseg[[:blank:]]*at\>/.area DSEG_A (ABS,DATA)\
.org/g' \
 -e 's/^[[:blank:]]*using[[:blank:]]*\([0-3]\)/; switch to bank \1 \
ar0=0+8*\1 \
ar1=1+8*\1 \
ar2=2+8*\1 \
ar3=3+8*\1 \
ar4=4+8*\1 \
ar5=5+8*\1 \
ar6=6+8*\1 \
ar7=7+8*\1 \
/g' \
 -e 's/\$nomod51//g' \
 -e 's/^for_sdcc = 0/for_sdcc = 1/g' \
 -e 's/\<s_gsinit\>/s_GSINIT/g'  | \
cat >${FILENAMEOUT}
#tee ${FILENAME}.s
fi

#
if false ; then 
echo 'A OR B' | \
sed -e 's/\<OR\>/|/g' | \
cat
fi

#sed -e 's/^[[:blank:]]*\([[:alnum:]_]*\)[[:blank:]]*bit[[:blank:]]*\([[:alnum:]_.]*\)/\1=\2 /g' | \
#sed -e 's/^[[:blank:]]*\([[:alnum:]_]*\)[[:blank:]]*bit[[:blank:]]*\(ul_fl[[:alnum:]_]*\)\.\([0-7]\)/\1=((\2-ul_flg)*8)+\3 /g' | \
