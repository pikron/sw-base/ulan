#include <string.h>
#include <syslog.h>
#include <nuttx/lib/modlib.h>

int ul_drv_module_initialize(FAR struct mod_info_s *modinfo);

void *ul_drv_ptr;

int main(int argc, char *argv[])
{
  int ret;
  ret = ul_drv_module_initialize(NULL);
  if (ret)
    syslog(LOG_ERR, "ul_drv_module_initialize error %d\n", ret);
  return 0;
}
